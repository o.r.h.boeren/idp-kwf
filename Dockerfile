FROM node:alpine
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package*.json ./
RUN npm i
RUN npm i bootstrap --save
COPY . ./
EXPOSE 3000
CMD ["npm", "start"]




