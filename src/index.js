import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Navigation from './components/Navigation.js'
import Welcome from './components/Welcome.js'
//import 'bootstrap/dist/css/bootstrap.min.css'

ReactDOM.render(
  <React.StrictMode>
    <Welcome/>
  </React.StrictMode>,
  document.getElementById('root')
);
//Render the navbar
ReactDOM.render(
  <Navigation/>, document.getElementById('nav')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
