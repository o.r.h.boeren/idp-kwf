import React from 'react'
import ReactDOM from 'react-dom'
import App from '../App.js'
import {Button} from 'react-bootstrap'

function Welcome(){
  return(
    <div className="content">
      <h1>Welcome</h1>
      <p>This project is made for the Interdisciplinary project of the Science honour academy. In this small survey, you'll find out what your relative risk for lung cancer is. In the future, this project can be expanded to more forms of cancer and give tailored advice on how you can decrease your risk.
</p><p>In the navigation bar, you can find websites with additional information on cancer and how to reduce your relative risk.</p>
      <Button variant="success" size="lg" onClick={startTest}>Start the test</Button>{' '}
    </div>
  )
}

function startTest(){
  ReactDOM.render(<App/>, document.getElementById("root"))
}

export default Welcome