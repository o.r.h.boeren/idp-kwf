import React from 'react'
import { Navbar,Nav,NavDropdown,Form,FormControl,Button } from 'react-bootstrap'


function Navigation(){
  return(<Navbar bg="light" expand="lg">
  <Navbar.Brand href="https://idp.olivierboeren.nl">IDP Cancer survey</Navbar.Brand>
  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="mr-auto">
      <Nav.Link href="https://www.kwf.nl">KWF</Nav.Link>
      <Nav.Link href="https://www.who.int/health-topics/cancer#tab=tab_1">WHO</Nav.Link>
      <Nav.Link href="https://www.iknl.nl/">IKNL</Nav.Link>
      <Nav.Link href="https://www.wkof.nl/ ">WKOF</Nav.Link>
    </Nav>
  </Navbar.Collapse>
</Navbar>)
}

export default Navigation