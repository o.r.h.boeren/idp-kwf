import * as w from "./weights.json"
import * as advice from "./advice.json"
import React from "react"
import Advice from './components/advice.js'


let weights = w.default //Removes the default container


export function processWeights(result){
  let res = 1
  console.log(w["default"])
  console.log(res)
  for(let a in result){
    let v = result[a]
    console.log(`${a} ${v}`)
    if(Array.isArray(v)){
      res *= processArray(a, v)
    }else{
      res *= processValue(a, v)
    }
    console.log(`res: ${res}`)
  }
  console.log(res)
  return res
}
function processArray(question, a){
  let res = 1
  // let v = "Pneumonia"
  // console.log(weights[question]["weights"][v])
  console.log(a)
  for(let i = 0; i < a.length; i++){
    let val = a[i]
    console.log(val)
    let r = weights[question]["weights"][val]
    res *= r
  }
  return res
}

function processValue(question, val){
  let r = weights[question]["weights"][val]
  return r
}

export function getAdvice(weights){
  if(weights <= 5){
    return (
      <div className="advice">
        <h1>Advice</h1>
        <p>Compared to people of your age and sex that experience none of the risk factors in the survey you are {weights.toFixed(2)} times as likely to be diagnosed with lung cancer.</p>
        <p>{advice.lungcancer[5]}</p>
      </div>)
  }else{
    return(
      <div className="advice">
        <h1>Advice</h1>
        <p>Compared to people of your age and sex that experience none of the risk factors in the survey you are {weights.toFixed(2)} times as likely to be diagnosed with lung cancer.</p>
        <p>{advice.lungcancer["otherwise"]}</p>
    </div>)
  }
}