import React, { Component } from "react";
import ReactDom from "react-dom"
import * as Survey from "survey-react";
import * as dp from "./dataProcessor.js"

import qs from "./questions.json"

import "survey-react/modern.css";
import "./index.css";

Survey.StylesManager.applyTheme("modern");

class App extends Component {
  constructor() {
    super();
    console.log("Hello world")
  }
  render() {    
  const survey = new Survey.Model(qs);

  survey.onComplete.add(function(res) {
    let risk = dp.processWeights(res.data)
    ReactDom.render(dp.getAdvice(risk), document.getElementById('root'))
  })

  return <Survey.Survey model={survey} />;
} 

}

export default App;
